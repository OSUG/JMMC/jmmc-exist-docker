FROM docker.io/existdb/existdb:6.2.0
# random iaidè8isf_1kjqh value to build a new image with a new id

# XAR built on Github shoud be tagged using vX.Y.Z for proper retrieval since next constants do only provide X.Y.Z part
ENV VOAR_VERSION=0.8
ENV DATAMODEL_VERSION=0.4
ENV A2P2W_VERSION=0.7
ENV OIVAL_VERSION=2.15.1
ENV JMMC_RESOURCES_VERSION=0.47.0
ENV SEARCHFTT_VERSION=1.5.30
ENV RELEASES_VERSION=1.1.6

ADD http://exist-db.org/exist/apps/public-repo/public/expath-ft-client-exist-lib-1.2.0.xar $EXIST_HOME/autodeploy/aa_expath-ft-client-exist-lib-1.2.0.xar
# yy or zz prefixes are added to ensure that next xar are loaded as last packages 
ADD http://exist-db.org/exist/apps/public-repo/public/shared-resources-0.9.1.xar $EXIST_HOME/autodeploy/yy_shared-resources.xar
ADD https://github.com/JMMC-OpenDev/jmmc-resources/releases/download/v$JMMC_RESOURCES_VERSION/jmmc-resources-$JMMC_RESOURCES_VERSION.xar $EXIST_HOME/autodeploy/yy_jmmc-ressources-$JMMC_RESOURCES_VERSION.xar

ADD https://github.com/JMMC-OpenDev/a2p2w/releases/download/v$A2P2W_VERSION/a2p2w-$A2P2W_VERSION.xar $EXIST_HOME/autodeploy/zz_a2p2w-$A2P2W_VERSION.xar
ADD https://github.com/JMMC-OpenDev/datamodels/releases/download/v$DATAMODEL_VERSION/datamodels-$DATAMODEL_VERSION.xar $EXIST_HOME/autodeploy/zz_datamodels-$DATAMODEL_VERSION.xar
ADD https://github.com/JMMC-OpenDev/oival/releases/download/v$OIVAL_VERSION/oival-$OIVAL_VERSION.xar $EXIST_HOME/autodeploy/zz_oival-$OIVAL_VERSION.xar
ADD https://github.com/JMMC-OpenDev/releases/releases/download/v$RELEASES_VERSION/releases-$RELEASES_VERSION.xar $EXIST_HOME/autodeploy/zz_releases-$RELEASES_VERSION.xar
ADD https://github.com/JMMC-OpenDev/searchftt/releases/download/v$SEARCHFTT_VERSION/searchftt-$SEARCHFTT_VERSION.xar $EXIST_HOME/autodeploy/zz_searchftt-$SEARCHFTT_VERSION.xar
ADD https://github.com/JMMC-OpenDev/voar/releases/download/v$VOAR_VERSION/voar-$VOAR_VERSION.xar $EXIST_HOME/autodeploy/zz_voar-$VOAR_VERSION.xar
